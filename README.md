# Bayes_Proportion_Estimation_2

In this rmd file, you can have a look at another proportion estimation using bayesian method. We use the same data about diabete II that was used in Bayes_Proportion_Estimation. Here are the questions in this exercise : 

1. Discuss about the a priori choice, is it reasonable ?

2. Thanks to previous studies, we know that the diabete prevalence is about $5\%$, with a variance around $0,01$. Based on parameters with Beta law, deduce a reasonable a priori law

3. Represent the previous a priori law and comment

4. Is this a priori law informative or not ?

5. Do the analysis of the results again when we had 2 diabetic patients among 10 :
  a. What becomes the a posteriori distribution
  b. Give a credibility interval at 95% and comment
  c. Is this informative a priori an advantage or a weakness ?



Author : Marion Estoup

E-mail : marion_110@hotmail.fr

December 2022
